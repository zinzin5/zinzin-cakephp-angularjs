<nav class="navbar navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand logo" href="ninja_files/ninja.html"><img
                    src="ninja_files/541073027bb52-53a7963e459b8-logo2.png" style="height:35px; width:auto;"></a>

            <div class="mobile-menu-toggle"><i class="fa fa-bars"></i></div>

            <!-- MOBILE NAV -->

            <div class="mobile-menu" style="height: 842px;">


                <ul>
                    <li class="active"><a href="ninja_files/ninja.html"><i class="fa fa-home"></i> Home</a></li>
                    <li class="dropdown ">
                        <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-star"></i> Popular <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://app.ninjamediascript.com/popular/week">for the Week</a></li>
                            <li><a href="http://app.ninjamediascript.com/popular/month">for the Month</a></li>
                            <li><a href="http://app.ninjamediascript.com/popular/year">for the Year</a></li>
                            <li><a href="http://app.ninjamediascript.com/popular">All Time</a></li>
                        </ul>
                    </li>


                    <li class="dropdown">
                        <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle categories"
                           data-toggle="dropdown"><i class="fa fa-folder-open"></i> Categories <b class="caret"></b></a>

                        <ul class="dropdown-menu">
                            <li>
                                <a href="http://app.ninjamediascript.com/category/animals">Animals</a>
                                <a href="http://app.ninjamediascript.com/category/architecture">Architecture</a>
                                <a href="http://app.ninjamediascript.com/category/cartoon">Cartoon</a>
                                <a href="http://app.ninjamediascript.com/category/comedy">Comedy</a>
                                <a href="http://app.ninjamediascript.com/category/comics">Comics</a>
                                <a href="http://app.ninjamediascript.com/category/family">Family</a>
                                <a href="http://app.ninjamediascript.com/category/film">Film</a>
                                <a href="http://app.ninjamediascript.com/category/food">Food</a>
                                <a href="http://app.ninjamediascript.com/category/music">Music</a>
                                <a href="http://app.ninjamediascript.com/category/news">News</a>
                                <a href="http://app.ninjamediascript.com/category/retro">Retro</a>
                                <a href="http://app.ninjamediascript.com/category/sports">Sports</a>
                                <a href="http://app.ninjamediascript.com/category/uncategorized">Uncategorized</a>
                                <a href="http://app.ninjamediascript.com/category/vehicles">Vehicles</a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown ">
                        <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle" data-toggle="dropdown"><i
                                class="fa fa-file-text"></i> More <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://app.ninjamediascript.com/pages/add-pages">Add Pages</a></li>

                        </ul>
                    </li>

                    <li><a href="http://app.ninjamediascript.com/random"><i class="fa fa-random"></i> Random</a></li>

                </ul>

                <!-- END MOBILE NAV -->


                <ul class="nav navbar-nav navbar-right">


                    <li class=""><a href="http://app.ninjamediascript.com/login">Login</a></li>

                    <li class=""><a href="http://app.ninjamediascript.com/signup">Sign up</a></li>

                    <li><a href="http://app.ninjamediascript.com/upload" class="upload-btn"><i
                                class="fa fa-cloud-upload"></i> Upload</a></li>
                </ul>

            </div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">

            <ul class="nav navbar-nav navbar-left nav-desktop">
                <!--li><a href="#" id="categories_open"><i class="fa fa-th-list"></i> Categories</a></li-->
                <li class="active"><a href="ninja_files/ninja.html"><i class="fa fa-home"></i><span> Home</span></a>

                    <div class="nav-border-bottom"></div>
                </li>
                <li class="dropdown ">
                    <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-star"></i><span> Popular </span><b class="caret"></b>

                        <div class="nav-border-bottom"></div>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="http://app.ninjamediascript.com/popular/week">for the Week</a></li>
                        <li><a href="http://app.ninjamediascript.com/popular/month">for the Month</a></li>
                        <li><a href="http://app.ninjamediascript.com/popular/year">for the Year</a></li>
                        <li><a href="http://app.ninjamediascript.com/popular">All Time</a></li>
                    </ul>
                </li>


                <li class="dropdown">
                    <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle categories"
                       data-toggle="dropdown"><i class="fa fa-folder-open"></i><span> Categories </span><b
                            class="caret"></b>

                        <div class="nav-border-bottom"></div>
                    </a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="http://app.ninjamediascript.com/category/animals">Animals</a>
                            <a href="http://app.ninjamediascript.com/category/architecture">Architecture</a>
                            <a href="http://app.ninjamediascript.com/category/cartoon">Cartoon</a>
                            <a href="http://app.ninjamediascript.com/category/comedy">Comedy</a>
                            <a href="http://app.ninjamediascript.com/category/comics">Comics</a>
                            <a href="http://app.ninjamediascript.com/category/family">Family</a>
                            <a href="http://app.ninjamediascript.com/category/film">Film</a>
                            <a href="http://app.ninjamediascript.com/category/food">Food</a>
                            <a href="http://app.ninjamediascript.com/category/music">Music</a>
                            <a href="http://app.ninjamediascript.com/category/news">News</a>
                            <a href="http://app.ninjamediascript.com/category/retro">Retro</a>
                            <a href="http://app.ninjamediascript.com/category/sports">Sports</a>
                            <a href="http://app.ninjamediascript.com/category/uncategorized">Uncategorized</a>
                            <a href="http://app.ninjamediascript.com/category/vehicles">Vehicles</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="fa fa-file-text"></i> More <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://app.ninjamediascript.com/pages/add-pages">Add Pages</a></li>

                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdownNotifi">
                    <a href="http://app.ninjamediascript.com/#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell"></i>
                    </a>
                </li>
                <li><a href="http://app.ninjamediascript.com/random" class="random"><i class="fa fa-random"></i></a>
                </li>
                <li><a href="http://app.ninjamediascript.com/upload" class="upload-btn upload-btn-desktop"><i
                            class="fa fa-cloud-upload"></i> Upload</a></li>


                <li class=""><a href="http://app.ninjamediascript.com/login" id="login-button-desktop">Login</a>

                    <div class="nav-border-bottom"></div>
                </li>
                <li class=""><a href="http://app.ninjamediascript.com/signup" id="signup-button-desktop">Sign up</a>

                    <div class="nav-border-bottom"></div>
                </li>
            </ul>

        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>