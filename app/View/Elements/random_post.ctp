<div id="random-bar">
    <div id="random-left"><i class="fa fa-angle-left"></i></div>
    <div id="random-right"><i class="fa fa-angle-right"></i></div>
    <div class="container random-container">
        <div id="random-slider">
            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/blood-puddle-pillows"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/blood-puddle-pillows.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/invisible-bookshelf"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/January2014/invisible-bookshelf.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img"
                   href="http://app.ninjamediascript.com/media/say-goodbye-to-your-friends-and-get-in-the-car"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/say-goodbye-to-your-friends-and-get-in-the-car.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/those-bastards"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/piano_robot.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/a-case-of-the-mondays"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/filled_with_excitement.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/defense"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/defense.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/how-to-draw-an-owl"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/how-to-draw-an-owl.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/oh-google"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/oh-google.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/meet-your-new-cubemate"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/cubemate_small.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/check-out-what-i-can-do"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/check-out-what-i-can-do.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/a-common-work-occurrence"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/January2014/a-common-work-occurrence.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/must-be-monday"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/February2014/must-be-monday.gif); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/lies"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/lies.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/this-just-in"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/December2013/cheeseburger-stabbing.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/pool-jumpers-trailer"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/January2014/pool-jumpers-trailer.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/nintendo-bed"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/February2014/nintendo-bed.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/stealth-mode"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/January2014/stealth-mode.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

            <div class="random-item">
                <a class="random-img" href="http://app.ninjamediascript.com/media/bare-necessities"
                   style="background-image:url(http://app.ninjamediascript.com/content/uploads/images/January2014/bare-necessities.jpg); display:block; min-height:100px; background-size:cover; background-position-y:center"></a>
            </div>

        </div>
    </div>
</div>