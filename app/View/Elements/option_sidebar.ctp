<div class="options_sidebar">

    <h2>Viewing Options:</h2>

    <div class="viewing_options">
        <i class="fa fa-th-list  active" data-url="view/list"></i>
        <i class="fa fa-th-large " data-url="view/grid_large"></i>
        <i class="fa fa-th" data-url="view/grid"></i>
    </div>
    <div style="clear:both"></div>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.viewing_options i').click(function(){
            window.location = $(this).data('url');
        });
    });
</script>