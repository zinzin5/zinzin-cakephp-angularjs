<h2>Latest Posts</h2>
<?php
  $posts = $this->requestAction(
    'posts/index/sort:updated/direction:asc/limit:5'
);
?>
<ol>
<?php foreach ($posts as $post): ?>
<li><?php echo $post['Post']['title']; ?></li> <?php endforeach; ?>
</ol>