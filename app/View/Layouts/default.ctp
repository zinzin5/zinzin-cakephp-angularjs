<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
?>
<!DOCTYPE html>
<html lang="en" ng-app="zinzinApp" ng-controller="PostsController">
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        <?php echo 'Zinzin'; ?>
    </title>
    <?php
    echo $this->Html->meta('icon');
    ?>
    <meta name="viewport" content="initial-scale=1">
    <?php echo $this->Html->css(['bootstrap.min.css',
        'font-awesome.min.css',
        'animate.min.css',
        'style.css',
        'zinzin/style.css'
    ]);
    ?>
    <!--[if lte IE 8]>
    <script type="text/javascript" src="zinzin/js/respond.min.js"></script>
    <![endif]-->
    <?php echo $this->Html->script(['jquery-1.11.1.min.js',
        'bootstrap.min.js',
        'masonry.pkgd.min.js',
        'imagesloaded.pkgd.min.js',
        'jquery.infinitescroll.min.js',
        'jquery.sticky.js',
        'jquery.fitvid.js',
        'jquery.timeago.js',
        'zinzin/navbarController.js',
        'zinzin/angular.min.js'
    ]);
    ?>

</head>
<body>
<div id="container">
    <div id="content">

        <?php echo $this->Session->flash(); ?>

        <?php echo $this->fetch('content'); ?>
    </div>
    <div id="footer">
        <?php echo $this->Html->link(
            $this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
            'http://www.cakephp.org/',
            array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
        );
        ?>
        <p>
            <?php echo $cakeVersion; ?>
        </p>
    </div>
</div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
