'zinzin/navbarController.js',

<?php
echo $this->Html->script('zinzin/PostController.js');
echo $this->element('navbar'); ?>
<style type="text/css">
    .notifications {
        margin-top: -7px;
        display: inline-block;
        float: right;
    }

    .dropdownNotifi .dropdown-menu {
        display: none;
        opacity: 1 !important;
        visibility: visible !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.dropdownNotifi').on('click', 'a.readNotifi', function (event) {
            event.stopPropagation();
            tc = $(this);
            var ID = tc.attr("id");
            $.ajax({
                type: 'POST',
                url: '/notification',
                data: {},
                success: function (response) {
                    $('.notifications').hide();
                }
            });
        });
    });
</script>
<style type="text/css">
    .gallery-sub-header {
        top: 0px;
    }

    .main_home_container {
        padding-top: 0px;
    }
</style>
<?php echo $this->element('random_post'); ?>
<div id="main_container" class="random_bar">


    <div class="navbar gallery-sub-header" style="z-index:9;">
        <div class="container">
            <div class="pull-left desc-follow">

                <p class="website_desc pull-left">Zinzin Social</p>

                <?php echo $this->element('home_social'); ?>

            </div>

            <div class="pull-right mobile-pull-right">
                <form class="navbar-form search-form col-xs-12" role="search" style="margin:0px; padding-top:4px;" action="{{ URL::to('/') }}" method="GET">
                    <div class="form-group">
                        <input type="text" class="form-control" name="search" placeholder="{{ Lang::get('lang.search') }}" style="-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px; height:30px;">

                    </div>
                </form>

                <div class="search_settings">
                    <i class="fa fa-search"></i>
                    <i class="fa fa-cog option-sidebar-toggle"><span class="cog-arrow-up">&#9650;</span><span class="cog-arrow-down">&#9660;</span></i>

                </div>
                <script>
                    $(document).ready(function(){
                        $('.option-sidebar-toggle').click(function(){
                            $(this).toggleClass('clicked');
                            $('.options_sidebar').slideToggle();
                            $('.cog-arrow-down').toggle();
                            $('.cog-arrow-up').toggle();
                        });

                        $('.fa-search').click(function(){
                            $('.search-form').toggle();
                            $('.search-form input').focus();
                        });
                    });
                </script>
            </div>

        </div>
    </div>


    <div class="container main_home_container main_home">


        <div class="col-md-8 col-lg-8"
             style="display:block; clear:both; margin:0px auto; padding:0px; padding-bottom:70px;">
            <?php echo $this->element('home_post'); ?>
            <!-- #media -->

        </div>

        <div class="col-md-4 col-lg-4" id="sidebar_container">

            <!-- OPTIONS BAR -->

            <?php echo $this->element('option_sidebar'); ?>

            <!-- END OPTIONS BAR -->
            <?php echo $this->element('sticky_post'); ?>



        </div>
        <div style="clear:both"></div>

    </div>

    <script type="text/javascript">


        $('document').ready(function () {

            $(".timeago").timeago();

            $.each($('.item'), function (index, value) {
                item_click_events($(value));
            });

        });


        $(document).ready(function () {

            var $container = $('#media');
            var no_more_media = "No More Media to Load.";
            var loading_more_media = "Loading More Media";

            $container.imagesLoaded(function () {
                $container.masonry();
            });

            $container.infinitescroll({

                    loading: {
                        finished: undefined,
                        finishedMsg: "<p>" + no_more_media + "</p>",
                        img: "data:image/gif;base64,R0lGODlhAQABAHAAACH5BAUAAAAALAAAAAABAAEAAAICRAEAOw==",
                        msg: null,
                        msgText: "<div class='loading'><i></i><i></i><i></i></div><p>" + loading_more_media + "</p>",
                        selector: null,
                        speed: 'fast',
                        start: undefined,
                    },

                    navSelector: "ul.pagination",
                    // selector for the paged navigation (it will be hidden)
                    nextSelector: "ul.pagination a:first",
                    // selector for the NEXT link (to page 2)
                    itemSelector: ".container #media .item",

                    animate: false,

                    bufferPx: 3000,
                    extraScrollPx: 15000,
                },

                function (newElements) {
                    // hide new items while they are loading
                    //var $newElems = $( newElements ).css({ opacity: 0 });

                    $.each($(newElements), function (index, value) {
                        item_click_events($(value));
                    });

                    $("#media").imagesLoaded(function () {
                        var $newElems = $(newElements);
                        $newElems.animate({opacity: 1});

                        $container.masonry('appended', $newElems, true);


                    });
                });

        });


        function toggle_gif(img, icon) {
            if ($(img).data('state') == 0) {
                play_gif(img, icon);
            } else {
                stop_gif(img, icon);
            }
        }

        function play_gif(img, icon) {
            $(img).attr('src', $(img).data('animation'));
            $(img).data('state', 1);
            $(icon).fadeOut();
        }

        function stop_gif(img, icon) {
            $(img).attr('src', $(img).data('original'));
            $(img).data('state', 0);
            $(icon).fadeIn();
        }

        function item_click_events(item) {

            $(item).find('.video_container').fitVids();

            item_gif_vine_events(item);

            media_like = $(item).find('.home-media-like');

            $(media_like).click(function () {
                if ($(this).data('authenticated') == false) {
                    window.location = 'http://app.ninjamediascript.com/signup';
                }
                this_object = $(this);
                $(this).children('i').removeClass('animated').removeClass('rotateIn');
                $(this_object).toggleClass('active');
                var like_count = $(this_object).parent().find('.home-like-count span');
                if ($(this_object).hasClass('active')) {
                    $(this_object).children('i').addClass('animated').addClass('bounceIn');
                    like_count.text(parseInt(like_count.text()) + 1);
                } else {
                    like_count.text(parseInt(like_count.text()) - 1);
                }
                $.post("http://app.ninjamediascript.com/media/add_like", {media_id: $(this).data('id')}, function (data) {

                });
            });

        }

        function item_gif_vine_events(object) {

            var gifs = $(object).find('.animated-gif .animation');
            var gif_play = $(object).find('.gif-play');

            gif_click(gifs);
            gif_click(gif_play);


            var vine = $(object).find('.vine-thumbnail');
            var vine_play = $(object).find('.vine-thumbnail-play');

            vine_click(vine);
            vine_click(vine_play);

        }

        function gif_click(object) {
            $(object).click(function () {
                animated_gif = $(this).parent('.animated-gif').find('.animation');
                play_icon = $(this).parent('.animated-gif').find('.gif-play');
                toggle_gif(animated_gif, play_icon);
            });
        }

        function vine_click(object) {
            $(object).click(function () {
                var embed = $(this).data('embed');
                $(this).parent('.video_container').html('<iframe class="vine-embed" src="' + embed + '" width="100%" height="600" frameborder="0"></iframe>');
            });
        }


    </script>


</div>

<div id="footer">
    © 2015 ZinZin
</div>

<?php echo $this->Html->script(['jquery.noty.js',
    'default.js',
    'top.js',
    'nprogress.js'
]);
?>

<script type="text/javascript">
    $('document').ready(function () {

        $('.dropdown').hover(function () {
            $(this).addClass('open');
        }, function () {
            $(this).removeClass('open');
        });
        $('.dropdownNotifi').hover(function () {
            $(this).removeClass('open');
        });
        $('.dropdownNotifi a').click(function () {
            $(this).parent().find('.dropdown-menu').toggle();
        });
        NProgress.start();


        var slide_pos = 1;

        $('#random-right').click(function () {
            if (slide_pos < 3) {
                $('#random-slider').css('left', parseInt($('#random-slider').css('left')) - parseInt($('.random-container').width()) - 28 + 'px');
                slide_pos += 1;
            }
        });

        $('#random-left').click(function () {
            if (slide_pos > 1) {
                $('#random-slider').css('left', parseInt($('#random-slider').css('left')) + parseInt($('.random-container').width()) + 28 + 'px');
                slide_pos -= 1;
            }
        });


    });


    $(window).load(function () {
        NProgress.done();
    });

    $(window).resize(function () {
        jquery_sticky_footer();
    });


    $(window).bind("load", function () {
        jquery_sticky_footer();
    });

    function jquery_sticky_footer() {
        var footer = $("#footer");
        var pos = footer.position();
        var height = $(window).height();
        height = height - pos.top;
        height = height - footer.outerHeight();
        if (height > 0) {
            footer.css({'margin-top': height + 'px'});
        }
    }

    /********** Mobile Functionality **********/

    var mobileSafari = '';

    $(document).ready(function () {
        $('.mobile-menu-toggle').click(function () {
            $('.mobile-menu').toggle();
            $('body').toggleClass('mobile-margin').toggleClass('body-relative');
            $('.navbar').toggleClass('mobile-margin');
        });


        // Assign a variable for the application being used
        var nVer = navigator.appVersion;
        // Assign a variable for the device being used
        var nAgt = navigator.userAgent;
        var nameOffset, verOffset, ix;


        // First check to see if the platform is an iPhone or iPod
        if (navigator.platform == 'iPhone' || navigator.platform == 'iPod') {
            // In Safari, the true version is after "Safari" 
            if ((verOffset = nAgt.indexOf('Safari')) != -1) {
                // Set a variable to use later
                mobileSafari = 'Safari';
            }
        }

        // If is mobile Safari set window height +60
        if (mobileSafari == 'Safari') {
            // Height + 60px
            $('.mobile-menu').css('height', (parseInt($(window).height()) + 60) + 'px');
        } else {
            // Else use the default window height
            $('.mobile-menu').css('height', $(window).height());
        }
        ;

    });

    $(window).resize(function () {
        // If is mobile Safari set window height +60
        if (mobileSafari == 'Safari') {
            // Height + 60px
            $('.mobile-menu').css('height', (parseInt($(window).height()) + 60) + 'px');
        } else {
            // Else use the default window height
            $('.mobile-menu').css('height', $(window).height());
        }
        ;
    });

    /********** End Mobile Functionality **********/


</script>


<script>
</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-7', 'auto');
    ga('send', 'pageview');

</script>
