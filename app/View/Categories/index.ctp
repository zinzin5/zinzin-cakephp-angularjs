<h1>Blog categories</h1>
<table>
	<tr>
		<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th> 
		<th><?php echo $this->Paginator->sort('name', 'Name'); ?></th>
		<th><?php echo $this->Paginator->sort('description', 'Description'); ?></th>
		<th><?php echo $this->Paginator->sort('created', 'Created'); ?></th>
	</tr>
    <!-- Here is where we loop through our $posts array, printing out post info -->
<?php foreach ($categories as $post): ?> <tr>
<tr>
<td><?php echo $post['Category']['id']; ?></td> <td>
<?php
echo $this->Html->link(
$post['Category']['name'],
array('action' => 'posts', $post['Category']['id'])
);
?>
</td>
<td>
	<?php echo $post['Category']['description']; ?> 
</td> 
<td>
	<?php echo $post['Category']['created']; ?> 
</td>
 <td>
<?php
echo $this->Form->postLink( 'Delete',
array('action' => 'delete', $post['Category']['id']),
array('confirm' => 'Are you sure?') );
?>
<?php
echo $this->Html->link( 'Edit',
array('action' => 'edit', $post['Category']['id']) );
?>
</td>
</tr>
<?php endforeach; ?>
<?php unset($post); ?> 
</table>