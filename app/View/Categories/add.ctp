<!-- File: /app/View/Posts/add.ctp -->
<h1>Add Category</h1>
<?php
echo $this->Form->create('Category', array(
	'inputDefaults' => array(
		'div' => 'form-group',
		'wrapInput' => false,
		'class' => 'form-control'
	),
	'class' => 'well'));
echo $this->Form->input('name');
echo $this->Form->input('description', array('rows' => '3')); 

echo $this->Form->end('Save Category', array('div' => 'form-group', 
											'class' => 'btn btn-default'
	));
?>

