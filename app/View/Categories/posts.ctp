<!-- File: /app/View/Posts/index.ctp -->
<?php echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js')."\n";
echo $this->Html->css('bootstrap')."\n";
echo $this->Html->script('bootstrap')."\n";
?>
<h1>Blog posts</h1>
<table>
	<tr>
		<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th> 
		<th><?php echo $this->Paginator->sort('title', 'Title'); ?></th>
		<th><?php echo $this->Paginator->sort('created', 'Created'); ?></th>
	</tr>
    <!-- Here is where we loop through our $posts array, printing out post info -->
<?php foreach ($posts as $post): ?> <tr>
<tr>
<td><?php echo $post['Post']['id']; ?></td> <td>
<?php
echo $this->Html->link(
$post['Post']['title'],
array('action' => 'view', $post['Post']['id'])
);
?>
</td> 
<td>
	<?php echo $post['Post']['created']; ?> 
</td>
 <td>
<?php
echo $this->Form->postLink( 'Delete',
array('action' => 'delete', $post['Post']['id']),
array('confirm' => 'Are you sure?') );
?>
<?php
echo $this->Html->link( 'Edit',
array('action' => 'edit', $post['Post']['id']) );
?>
</td>
</tr>
<?php endforeach; ?>
<?php unset($post); ?> 
</table>
<?php echo $this->Html->link( 'Add Post in this Category',
array('controller' => 'posts', 'action' => 'add', $posts[0]['Post']['category_id']) ); ?>