<!-- File: /app/View/Posts/view.ctp -->

<?php
// app/View/Posts/view.ctp
$this->extend('/Common/view');
$this->assign('title', $post['Post']['title']);
$this->start('sidebar'); 
?>
<li>
<?php
echo $this->Html->link('Edit', array( 'action' => 'edit', $post['Post']['id']
)); ?>
</li>
<li>
<?php
echo $this->Html->link('Delete', array( 'action' => 'delete', $post['Post']['id']
), array('confirm' => 'Are you sure?') ); 
?>
</li>
<?php $this->end(); ?>
<p>
<small>Created: <?php echo $post['Post']['created']; ?></small>
</p> <p><?php echo h($post['Post']['body']); ?></p>

<?php foreach ($post['Tag'] as $tag) {
	echo $tag['name'];
} ?>