<!-- File: /app/View/Posts/index.ctp -->
<?php echo $this->Html->script('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js')."\n";
echo $this->Html->css('bootstrap')."\n";
echo $this->Html->script('bootstrap')."\n";
?>
<h1>Blog posts</h1>
<table>
	<tr>
		<th><?php echo $this->Paginator->sort('id', 'ID'); ?></th> 
		<th><?php echo $this->Paginator->sort('title', 'Title'); ?></th>
		<th><?php echo __('Category'); ?></th>
		<th><?php echo __('Tags'); ?></th>
		<th><?php echo $this->Paginator->sort('created', 'Created'); ?></th>
	</tr>
    <!-- Here is where we loop through our $posts array, printing out post info -->
<?php foreach ($posts as $post): ?> <tr>
<tr>
<td><?php echo $post['Post']['id']; ?></td> <td>
<?php
echo $this->Html->link(
$post['Post']['title'],
array('action' => 'view', $post['Post']['id'])
);
?>
</td> 
<td><?php echo $post['Category']['name']; ?></td>
<td><?php foreach ($post['Tag'] as $tag) {
	echo $tag['name']. ', ';
} ?> </td>
<td>
	<?php echo $post['Post']['created']; ?> 
</td>
 <td>
<?php
echo $this->Form->postLink( 'Delete',
array('action' => 'delete', $post['Post']['id']),
array('confirm' => 'Are you sure?') );
?>
<?php
echo $this->Html->link( 'Edit',
array('action' => 'edit', $post['Post']['id']) );
?>
</td>
</tr>
<?php endforeach; ?>
<?php unset($post); ?> 
</table>
<?php
// Shows the page numbers
// Shows the next and previous links
echo $this->Paginator->prev( '« Previous',
null,
null,
array('class' => 'disabled') );
echo	'   ';
echo $this->Paginator->numbers();
echo '   ';
echo $this->Paginator->next( 'Next »',
null,
null,
array('class' => 'disabled') );
// prints X of Y, where X is current page and Y is number of pages
echo  '<span>     </span>';
echo $this->Paginator->counter();
?>
</br>
<?php echo $this->Html->link( 'Add Post',
array('controller' => 'posts', 'action' => 'add') ); ?>

<?php
echo $this->element('related_post'); ?>