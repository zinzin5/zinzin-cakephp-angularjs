<!-- File: /app/View/Posts/add.ctp -->
<h1>Add Post</h1>
<?php
echo $this->Form->create('Post', array(
	'inputDefaults' => array(
		'div' => 'form-group',
		'wrapInput' => false,
		'class' => 'form-control'
	),
	'class' => 'well'));
echo $this->Form->input('title');
echo $this->Form->input('body', array('rows' => '3')); 
echo $this->Form->input('Post.Tag',array('label'=>'Tags', 'type'=>'select', 'multiple'=>true));
echo $this->Form->input('category_id', array('options' => $categories, 'default' => $category_id));

echo $this->Form->end('Save Post', array('div' => 'form-group', 
											'class' => 'btn btn-default'
	));
?>

