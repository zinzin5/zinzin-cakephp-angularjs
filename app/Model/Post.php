<?php
App::uses('AppModel', 'Model');
class Post extends AppModel{
	// public $validate = array(
	// 	'title' => array(
	// 	'rule' => 'notBlank'
	// 	),
	// 	'body' => array(
	// 	            'rule' => 'notBlank'
	// 	        )
	// );
	public $belongsTo = 'Category';
	var $hasAndBelongsToMany = array('Tag' => array(
			'className' => 'Tag',
			'joinTable' => 'posts_tags',
			'foreignKey' => 'post_id',
			'associationForeignKey' => 'tag_id',
			'unique' => 'keepExisting',
		));

	public function beforeSave($options = array()){
		foreach (array_keys($this->hasAndBelongsToMany) as $model){
			if(isset($this->data[$this->name][$model])){
				$this->data[$model][$model] = $this->data[$this->name][$model];
				unset($this->data[$this->name][$model]);
			}
		}
		return true;
	}

}