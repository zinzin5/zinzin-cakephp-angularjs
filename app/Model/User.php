<?php
class User extends AppModel{
	public $belongsTo = 'Group';
	public $displayField = 'first_name';
	public $hasMany = 'Category, Post';
}