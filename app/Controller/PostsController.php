<?php
App::uses('AppController', 'Controller');

class PostsController extends AppController { 

	public $scaffold = 'admin';

	public function admin_index(){
		$this->set('posts', $this->Post->find('all'));
		$this->set('sum', $this->Math->sumInt(3,4));
	}

	public $components = array(
		'Math','Paginator',
		'RequestHandler',
		'Cookie' => array('name' => 'CookieMonster') 
	);

	// public function beforeFilter() { 
	// 	$this->Auth->authorize = array('controller'); 
	// 	$this->Auth->loginAction = array(
 //        	'controller' => 'users',
 //        	'action' => 'login'
 //    	);
 //    	$this->Cookie->name = 'CookieMonster';
	// }

	public $helpers = array('Html', 'Form');

	public function index() {
		//$this->set('posts', $this->Post->find('all'));
		$this->Paginator->settings = array(
	        'limit' => 4,
	        'maxLimit' => 10
		);
		$posts = $this->paginate();
		$this->set(compact('posts'));
		$this->set('_serialize', array('posts'));
		if ($this->request->is('requested')) {
			return $posts; 
		}
        $this->set('posts', $posts);
	}

	// public function json(){
	// 	$this->set(compact('posts'));
	// 	$this->set('_serialize', array('posts'));
	// 	if ($this->request->is('requested')) {
	// 		return $posts; 
	// 	}
 //        $this->set('posts', $posts);	
	// }

	public function view($id = null) { 
		if (!$id) {
			throw new NotFoundException(__('Invalid post')); 
		}
		$post = $this->Post->findById($id); 
		if (!$post) {
			throw new NotFoundException(__('Invalid post')); 
		}
		$this->set('title_for_layout', $post['Post']['title']);
        $this->set('post', $post);
    }

    public function add($category_id = null) {
    	$this->set('title_for_layout', 'Add new Post');
    	if(!$category_id){
    		$this->set('category_id', 1);
    	}else{
    		$this->set('category_id', $category_id);
    	}
    	$categories = $this->Post->Category->find('list', array('conditions' => array('actived' => 1), 'fields' => array('Category.name')));
    	$tags = $this->Post->Tag->find('list');
    	$this->set(compact('tags'));
    	if($categories){
    		$this->set('categories', $categories);
    	}
		if ($this->request->is('post')) {
			$this->Post->create();
			$this->Post->validator()->remove('Tag');
			if ($this->Post->save($this->request->data)) {
			        //$this->Session->setFlash(__('Your post has been saved.'));
			return $this->redirect(array('action' => 'index', 'controller' => 'posts')); 
			}
			$this->Session->setFlash(__('Unable to add your post.'));
		}
	}

	public function edit($id = null){
		if(!$id){
			throw new NotFoundException(__('Invalid post'));
		}

		$post = $this->Post->findById($id);
		$categories = $this->Post->Category->find('list', array('conditions' => array('actived' => 1), 'fields' => array('Category.name')));
		if($categories){
    		$this->set('categories', $categories);
    	}
		if(!$post){
			throw new NotFoundException(__("Error Processing Request"));
		}

		if ($this->request->is(array('post', 'put'))) { 
			$this->Post->id = $id;
			if ($this->Post->save($this->request->data)) {
	            $this->Session->setFlash(__('Your post has been updated.'));
				return $this->redirect(array('action' => 'index')); 
			}
			$this->Session->setFlash(__('Unable to update your post'));
		}
		if (!$this->request->data) { 
			$this->request->data = $post;
		}
	}

	public function delete($id) {
		if ($this->request->is('get')) {
			throw new MethodNotAllowedException(); 
		}
		if ($this->Post->delete($id)) { 
			$this->Session->setFlash(
		            __('The post with id: %s has been deleted.', h($id))
		        );
		} else { 
			$this->Session->setFlash(
		            __('The post with id: %s could not be deleted.', h($id))
		        );
		}
		return $this->redirect(array('action' => 'index')); 
	}
}