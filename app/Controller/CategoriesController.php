<?php
class CategoriesController extends AppController{

	public  $components = array(
		'Paginator'
	);
	public $scaffold;
	public function index(){
		$this->Paginator->settings = array(
	        'limit' => 4,
	        'maxLimit' => 10
		);
		$categories = $this->paginate();
		$this->set(compact('categories'));
		$this->set('_serialize', array('categories'));
		if ($this->request->is('requested')) {
			return $categories; 
		}
        $this->set('categories', $categories);
	}

	public function posts($id = null){
		if (!$id) {
			throw new NotFoundException(__('Invalid category')); 
		}
		$posts = $this->Category->Post->find('threaded', array(
				'conditions' => array('category_id' => $id)
			));
		if (!$posts) {
			throw new NotFoundException(__('Invalid category')); 
		}
		$this->set('posts', $posts);
	}

	public function add(){
		$this->set('title_for_layout', 'Add new Category');
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
			       $this->Session->setFlash(__('Your post has been saved.'));
			return $this->redirect(array('action' => 'index', 'controller' => 'categories')); 
			}
			$this->Session->setFlash(__('Unable to add your category.'));
		}
	}

}