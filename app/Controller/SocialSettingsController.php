<?php
App::uses('AppController', 'Controller');

class SocialSettingsController extends AppController {

    public function getsettings(){
        $settings = $this->SocialSetting->findById('1');
        $this->set(compact('settings'));
        $this->set('_serialize', array('settings'));
        if ($this->request->is('requested')) {
            return $settings;
        }
        $this->set('settings', $settings);

    }
}