'use strict';

angular.module('zinzinApp', [])
    .directive( 'zinzinApp', [ '$http', '$sce', function ( $http, $sce ) {
        return {
            replace: true,
            restrict: "E",
            scope: {
                url: '@'
            }
        }
    }])
.controller('PostsController', function ($http, $scope) {
        $http.get('http://api.zuilen.tk/data/getnewdataapi').
            success(function(data, status, headers, config) {
                $scope.posts = data;
            }).
            error(function(data, status, headers, config) {
                // log error
            });
    });